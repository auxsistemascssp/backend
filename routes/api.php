<?php

use App\Exports\DepartamentExport;
use App\Http\Controllers\Api\v1\EmployeeStateController;
use App\Http\Controllers\Api\v1\auth\AuthController;
use App\Http\Controllers\Api\v1\auth\LoginController;
use App\Http\Controllers\Api\v1\CarouselItemController;
use App\Http\Controllers\Api\v1\ChargeController;
use App\Http\Controllers\Api\v1\UserController;
use App\Http\Controllers\Api\v1\DepartamentController;
use App\Http\Controllers\Api\v1\DocumentController;
use App\Http\Controllers\Api\v1\DocumentTypeController;
use App\Http\Controllers\Api\v1\EmployeeController;
use App\Http\Controllers\Api\v1\exports\ChargeController as ExportsChargeController;
use App\Http\Controllers\Api\v1\exports\DepartamentController as ExportsDepartamentController;
use App\Http\Controllers\Api\v1\exports\EmployeeController as ExportsEmployeeController;
use App\Http\Controllers\Api\v1\GenderController;
use App\Http\Controllers\Api\v1\imports\ChargeController as ImportsChargeController;
use App\Http\Controllers\Api\v1\imports\DepartamentController as ImportsDepartamentController;
use App\Http\Controllers\Api\v1\imports\EmployeeController as ImportsEmployeeController;
use App\Http\Controllers\Api\v1\LinkCategoryController;
use App\Http\Controllers\Api\v1\LinkController;
use App\Http\Controllers\Api\v1\PermissionController;
use App\Http\Controllers\Api\v1\RoleController;
use App\Http\Controllers\Api\v1\PhoneDirectoryController;
use App\Http\Controllers\Api\v1\PhotoAlbumController;
use App\Http\Controllers\Api\v1\PostController;
use App\Http\Controllers\Api\v1\PostStateController;
use App\Http\Controllers\Api\v1\PostTypeController;
use App\Http\Controllers\Api\v1\PqrsController;
use App\Http\Controllers\Api\v1\PqrsStateController;
use App\Http\Controllers\Api\v1\PqrsTypeController;
use App\Http\Controllers\Api\v1\ProcessController;
use App\Http\Controllers\Api\v1\ResourceTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\v1\show\CarouselItemController as ShowCarouselController;
use App\Http\Controllers\Api\v1\show\DepartamentController as ShowDepartamentController;
use App\Http\Controllers\Api\v1\show\DocumentController as ShowDocumentController;
use App\Http\Controllers\Api\v1\show\LinkController as ShowLinkController;
use App\Http\Controllers\Api\v1\show\PostController as ShowPostController;
use App\Http\Controllers\Api\v1\show\PhoneDirectoryController as ShowPhoneDirectoryController;
use App\Http\Controllers\Api\v1\show\PhotoAlbumController as ShowPhotoAlbumController;
use App\Http\Controllers\Api\v1\show\VideoController as ShowVideoController;
use App\Http\Controllers\Api\v1\VideoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function(Request  $request){
    
});

Route::prefix('v1')->group(function(){
    
    Route::prefix('auth')->group(function(){

        Route::post('login', [LoginController::class, 'login'])->name('login');
    });

    Route::get('/image', [EmployeeController::class, 'getDefaultImg']);
    

    Route::get('departamentsList', [ShowDepartamentController::class, 'index']);
    
    Route::get('pqrsTypesList', [PqrsTypeController::class, 'index']);

    Route::get('users/getClientIp', [UserController::class, 'getClientIp']);
    
    Route::get('carousels', [ShowCarouselController::class, 'index']);
    
    Route::get('linksList', [ShowLinkController::class, 'index']);    
    Route::get('linksList/categories', [ShowLinkController::class, 'search']);    
    
    Route::get('documentList', [ShowDocumentController::class, 'index']);    
    
    Route::get('photoAlbumList', [ShowPhotoAlbumController::class, 'index']);    
    Route::get('photoAlbumList/{photoAlbum}', [ShowPhotoAlbumController::class, 'show']);    
    
    Route::get('videoList', [ShowVideoController::class, 'index']);    
    Route::get('videoList/{video}', [ShowVideoController::class, 'show']);    
    
    Route::get('postsList/{post}', [ShowPostController::class, 'show']);    
    Route::get('postsList', [ShowPostController::class, 'index']);    
    Route::get('posts/lastest', [ShowPostController::class, 'lastestPosts']);
    Route::get('ads/getPopup', [ShowPostController::class, 'getPopup'])->name('ad.getPopUo');
    
    Route::get('phoneDirectoryList', [ShowPhoneDirectoryController::class, 'index']);    
    
    Route::get('employees/birthdays', [EmployeeController::class, 'showBirthdays']);
    Route::resource('pqrs', PqrsController::class)->only('store');

    Route::middleware('auth:sanctum')->group(function(){
        
        Route::prefix('me')->group(function(){

            Route::get('/', [AuthController::class, 'me']);
            Route::put('/', [AuthController::class, 'updateMyInfo']);
            Route::put('password', [AuthController::class, 'updateMyPassword']);
            Route::post('logout', [AuthController::class, 'logout']);
            Route::get('permissions', [AuthController::class, 'permisssions']);
        });

        Route::post('roles/{role}/updatePermissions', [RoleController::class, 'updatePermissions']);
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);
        Route::resource('departaments', DepartamentController::class);
        
        Route::resource('genders', GenderController::class);

        Route::delete('users/{user}/deletePhoto', [UserController::class, 'deletePhoto']);
        Route::post('users/{user}/updatePhoto', [UserController::class, 'updatePhoto']);
        Route::post('users/{user}/changePassword', [UserController::class, 'updatePassword']);
        Route::resource('users', UserController::class);
        
        Route::resource('charges', ChargeController::class);
        Route::resource('chargePhone', PhoneDirectoryController::class);
        Route::resource('employees', EmployeeController::class);
        Route::resource('employeeStates', EmployeeStateController::class);
        
        Route::delete('posts/{post}/deleteCover', [PostController::class, 'deleteCover'])->name('posts.destroy.cover');
        Route::post('posts/{post}/updateCover', [PostController::class, 'updateCover'])->name('posts.update.cover');
        Route::resource('postsTypes', PostTypeController::class);
        Route::resource('postsStates', PostStateController::class);
        
        Route::resource('posts', PostController::class);
    
        Route::prefix('events')->group(function(){
            Route::post('/', [PostController::class, 'storeEvent'])->name('even.store');
            Route::put('/{event}', [PostController::class, 'updateEvent'])->name('even.update');
        });
        
        Route::prefix('ads')->group(function(){
            Route::post('/', [PostController::class, 'storeAd'])->name('ad.store');
            Route::put('/{ad}', [PostController::class, 'updateAd'])->name('ad.update');
        });
    
        Route::resource('linkCategories', LinkCategoryController::class);
        Route::resource('links', LinkController::class);
        Route::delete('links/{link}/deleteCover', [LinkController::class, 'deleteCover'])->name('link.destroy.cover');
        Route::post('links/{link}/updateCover', [LinkController::class, 'updateCover'])->name('link.update.cover');
        
        Route::post('carouselItem/{carouselItem}/updateResource', [CarouselItemController::class, 'updateResource']);
        Route::post('carouselItem/{carouselItem}/updateCover', [CarouselItemController::class, 'updateCover']);
        Route::delete('carouselItem/{carouselItem}/deleteCover', [CarouselItemController::class, 'deleteCover']);
        Route::delete('carouselItem/{carouselItem}/deleteResource', [CarouselItemController::class, 'deleteResource']);
        Route::resource('carouselItem', CarouselItemController::class);
        
        Route::post('photoAlbum/{photoAlbum}/uploadPhoto', [PhotoAlbumController::class, 'storePhoto']);
        Route::delete('photoAlbum/{photoAlbum}/{photo}/deletePhoto', [PhotoAlbumController::class, 'destroyPhoto']);
        Route::resource('photoAlbum', PhotoAlbumController::class);
        
        Route::delete('videos/{video}/deleteVideoMedia', [VideoController::class, 'deleteVideoMedia']);
        Route::post('videos/{video}/uploadVideoMedia', [VideoController::class, 'uploadVideoMedia']);
        Route::resource('videos', VideoController::class);
        
        Route::resource('pqrs', PqrsController::class)->except('store');
        Route::resource('pqrsStates', PqrsStateController::class);
        Route::resource('pqrsTypes', PqrsTypeController::class);

        Route::resource('resourceTypes', ResourceTypeController::class);

        Route::delete('documents/{document}/deleteFile', [DocumentController::class, 'deleteFile']);
        Route::post('documents/{document}/uploadFile', [DocumentController::class, 'uploadFile']);
        Route::resource('documents', DocumentController::class);

        Route::resource('documentTypes', DocumentTypeController::class);
        Route::resource('process', ProcessController::class);
        // Export
        Route::prefix('exports')->group(function(){
            Route::get('employeeTemplate', [ExportsEmployeeController::class, 'exportUploadTemplate']);
            Route::get('departamentTemplate', [ExportsDepartamentController::class, 'exportUploadTemplate']);
            Route::get('chargeTemplate', [ExportsChargeController::class, 'exportUploadTemplate']);
        });
        
        // Imports
        Route::prefix('imports')->group(function(){
            Route::post('employeeTemplate', [ImportsEmployeeController::class, 'uploadTemplate']);
            Route::post('departamentTemplate', [ImportsDepartamentController::class, 'uploadTemplate']);
            Route::post('chargeTemplate', [ImportsChargeController::class, 'uploadTemplate']);
        });
    });    
});
