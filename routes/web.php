<?php
use App\Http\Controllers\Api\v1\auth\LoginController;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('sanctum/csrf-cookie', [LoginController::class, 'show']);

Route::get('condolence', function(Request $request){

    return view('emails.condolenceEmployee');
});

Route::get('condolence2', function(Request $request){

    $employees = Employee::with(['user', 'charge'])
    ->find(4322);
    
    return view('emails.condolenceEmployee2')
    ->with('employee', $employees);
});