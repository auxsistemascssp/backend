<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Charge extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'departament_id',
        'email'
    ];

    /**
     * Get the charge's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }


    /**
     * Relationship with Departament
     * 
     * @return BelongsTo
     */
    public function departament(): BelongsTo
    {
        return $this->belongsTo(Departament::class, 'departament_id');
    }

    /**
     * Relationship with ChargePhone
     * 
     * @return HasMany
     */
    public function phones(): HasMany
    {
        return $this->hasMany(ChargePhone::class, 'charge_id');
    }

    /**
     * Relationship with Employee
     * 
     * @return HasMany
     */
    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class, 'charge_id');
    }


    /**
     * 
     */
    public function scopeName($query, $nameQuery)
    {
        if($nameQuery){
            return $query->where('name', 'like', "%{$nameQuery}%");
        }
    }

    /**
     * 
     */
    public function scopeEmail($query, $emailQuery)
    {
        if($emailQuery){
            return $query->orWhere('email', 'like', "%{$emailQuery}%");
        }
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('name', 'like', "%{$s}%")
            ->orWhere('email', 'like', "%{$s}%")
            ->orWhereHas('phones', function($query) use ($s){
                $query->where('ext', 'like', "%{$s}%");
            });
        }
    }
}
