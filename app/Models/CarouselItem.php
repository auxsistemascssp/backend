<?php

namespace App\Models;

use App\Traits\HasCover;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CarouselItem extends Model
{
    use HasFactory, HasCover;

    protected $fillable = [
        'name',
        'description',
        'position',
        'src',
        'is_active',
        'cover',
        'resource_type_id'
    ];

    /**
     * Get the Carousel's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return ($value === "") ? '' : env('APP_URL')."/".$value;
    }

    /*
     *
     * 
    */
    public function resource(): BelongsTo
    {
        return $this->belongsTo(ResourceType::class, 'resource_type_id');
    }
}
