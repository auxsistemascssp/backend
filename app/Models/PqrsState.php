<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PqrsState extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    /**
     * Get the PQRS state's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * 
     */
    public function pqrs(): HasMany
    {
        return $this->hasMany(Pqrs::class, 'pqrs_state_id');
    }
}
