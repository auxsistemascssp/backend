<?php

namespace App\Models;

use App\Traits\HasCover;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Link extends Model
{
    use HasFactory, HasCover;

    protected $fillable = [
        'name',
        'cover',
        'to',
        'target',
        'link_category_id',
        'is_linked_home'
    ];

    /**
     * Get the Link's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return ($value === "") ? '' : env('APP_URL')."/".$value;
    }

    /**
     * 
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(LinkCategory::class, 'link_category_id');
    }

    /**
     * 
     */
    public function scopeLinked($query, $linked)
    {
        if($linked){
            return $query->where('is_linked_home', '=', true);
        }
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * @param \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('name', 'like', "%{$s}%")
            ->orWhereHas('category', function($query) use ($s){
                $query->where('name', 'like', "%{$s}%");
            });
        }
    }
}
