<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PhotoAlbum extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    /**
     * Get the PhotoAlbum's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * 
     */
    public function photos(): HasMany
    {
        return $this->hasMany(Photo::class, 'photo_album_id');
    }

    /**
     * 
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', 'like', "%{$name}%");
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * @return @param \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('name', 'like', "%{$s}%")
            ->orWhere('description', 'like', "%{$s}%");
        }
    }
}
