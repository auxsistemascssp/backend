<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CategoryPermission extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'subject'
    ];

    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'category_permission_id');
    } 
}
