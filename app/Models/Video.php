<?php

namespace App\Models;

use App\Traits\HasFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory, HasFile;

    protected $fillable = [
        'name',
        'cover',
        'src'
    ];

    /**
     * Get the Video's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * 
     */
    public function scopeName($query, $name)
    {
        if($name){
            return $query->where('name', 'like', "%{$name}%");
        }
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s){
        if($s){
            return $query->where('name', 'like', "%{$s}%");
        }
    }
}
