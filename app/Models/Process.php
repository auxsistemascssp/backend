<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Process extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    /**
     * Relationship with Document
     * 
     * @return HasMany
     */
    public function documents(): HasMany
    {
        return $this->hasMany(Document::class, 'process_id');
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String s
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s !== ''){
            return $query->where('name', 'like', "%{$s}%");
        }
    }
}
