<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Permission\Models\Permission as ModelsPermission;

class Permission extends ModelsPermission
{
    use HasFactory;

    protected $fillable = [
        'crud_action_id'
    ];

    /**
     * 
     */
    public function action():BelongsTo
    {
        return $this->belongsTo(CrudAction::class, 'crud_action_id');
    }

    /**
     * 
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(CategoryPermission::class, 'category_permission_id');
    }
}
