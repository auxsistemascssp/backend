<?php

namespace App\Models;

use App\Traits\HasCover;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Document extends Model
{
    use HasFactory, HasCover;

    protected $fillable = [
        'code',
        'name', 
        'src',
        'document_type_id',
        'departament_id',
        'process_id',
        'version',
        'state',
        'origin',
    ];

    /**
     * Get the charge's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the src url
     *
     * @param  string  $value
     * @return string
     */
    public function getSrcAttribute($value)
    {
        return ($value === "") ? '' : env('APP_URL')."/".$value;
    }

    /** 
     * Relationship with DocumenetType
     * 
     * @return BelongsTo
    */
    public function type(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }

    /**
     * Relationship with Process
     * 
     * @return BelongsTo
     */
    public function process(): BelongsTo
    {
        return $this->belongsTo(Process::class, 'process_id');
    }

    /**
     * Relationship with Departament
     * 
     * @return BelongsTo
     */
    public function departament(): BelongsTo
    {
        return $this->belongsTo(Departament::class, 'departament_id');
    }

    /**
     * Scope search
     */
    public function scopeSearch($query, $q)
    {
        if($q){
            return $query->where('code', 'like', "%{$q}%")
            ->orWhere('origin', 'like', "%{$q}%")
            ->orWhere('name', 'like', "%{$q}%")
            ->orWhereHas('departament', function($query) use ($q){
                $query->where('name', 'like', "%{$q}%");
            })
            ->orWhereHas('process', function($query) use ($q){
                $query->where('name', 'like', "%{$q}%");
            });
        }
    }
}
