<?php

namespace App\Models;

use App\Traits\HasCover;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    use HasFactory, HasCover;

    protected $fillable = [
        'title',
        'slug',
        'body',
        'short_description',
        'cover',
        'href',
        'user_id',
        'post_type_id',
        'post_state_id',
        'start_at',
        'finish_at'
    ];

    /**
     * Get the post's title
     * 
     * @param  string  $value
     * @return string
     */
    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return ($value === "") ? '' : env('APP_URL')."/".$value;
    }

    /**
     * 
     */
    public function autor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * 
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(PostType::class, 'post_type_id');
    }

    /**
     * 
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(PostState::class, 'post_state_id');
    }

    /**
     * 
     */
    public function scopeType($query, $type)
    {
        if(is_array($type))
            return $query->whereIn('post_type_id', $type);

        if($type)
            return $query->where('post_type_id', '=', $type);
    }

    /**
     * 
     */
    public function scopeTitle($query, $title)
    {
        if($title){
            return $query->where('title', 'like', "%{$title}%");        
        }
    }

    /**
     * 
     */
    public function scopeState($query, $state)
    {
        if($state)
            return $query->where('post_state_id', '=', $state);
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * 
     * @return @param \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('title', 'like', "%{$s}%")
            ->orWhere('body', 'like', "%{$s}%")
            ->orWhereHas('state', function($query) use ($s){
                $query->where('name', 'like', "%{$s}%");
            });
        }
    } 
}
