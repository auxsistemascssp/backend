<?php

namespace App\Models;

use App\Traits\HasCover;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, HasCover;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'last_name',
        'identification_number',
        'email',
        'gender_id',
        'username',
        'password',
        'cover'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * 
     */
    public $guard_name = 'sanctum';

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * Set the user's last name.
     *
     * @param  string  $value
     * @return void
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = strtolower($value);
    }
    
   /**
     * Get the user's name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
   
    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return ($value === "") ? '' : env('APP_URL')."/".$value;
    }

    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getLastNameAttribute($value)
    {
        return ucwords($value);
    }

    /**
     * Relationship with User
     * 
     * @return HasOne
     */
    public function employee(): HasOne
    {
        return $this->hasOne(Employee::class, 'user_id');
    }

    /*
    *
    */
    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * 
     * @return @param \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('name', 'like', "%{$s}%")
            ->orWhere('last_name', 'like', "%{$s}%")
            ->orWhere('email', 'like', "%{$s}%")
            ->orWhere('username', 'like', "%{$s}%");
        }
    }
}
