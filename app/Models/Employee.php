<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $fillable = [
        'employee_state_id',
        'employee_state_id',
        'charge_id',
        'phone',
        'birthday',
        'dead_day',
    ];
    
    /**
     * ======================
     * GETTERS AND SETTERS
     * ======================
     */
    public function getFormatDate($field)
    {
        $date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->{$field});

        $month = ucfirst($date->monthName);
        $day = $date->day;
        $year = $date->year;

        return "{$month}, {$day} de {$year}";
    }

    
    /**
     * Relationship with User
     * 
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relationship with Charge
     * 
     * @return BelongsTo
     */
    public function charge(): BelongsTo
    {
        return $this->belongsTo(Charge::class, 'charge_id');
    }

    /**
     *  Relationship with EmployeeState
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function employeeState(): BelongsTo
    {
        return $this->belongsTo(EmployeeState::class, 'employee_state_id');
    }


    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('employee_state_id', '=', 2);
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s !== ''){
            return $query->where('phone', 'like', "%{$s}%")
            ->orWhereHas('user', function($query) use ($s){
                $query->where('name', 'like', "%{$s}%")
                ->orWhere('last_name', 'like', "%{$s}%")
                ->orWhere('identification_number', 'like', "%{$s}%");
            })
            ->orWhereHas('charge', function($query) use ($s){
                $query->where('name', 'like', "%{$s}%"); 
            });
        }
    }
}