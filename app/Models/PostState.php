<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostState extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
        'text_color',
        'bg_color'
    ];

    /**
     * Get the postState's name
     * 
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }
}
