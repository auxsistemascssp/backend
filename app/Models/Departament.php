<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Departament extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'parent_id',
    ];

    /**
     * Get the departament's name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }


    /**
     * Relationship with Charge
     * 
     * @return HasMany
     */
    public function charges(): HasMany
    {
        return $this->hasMany(Charge::class, 'departament_id');
    }

    /***
     * 
     */
    public function departaments(): HasMany
    {
        return $this->hasMany(Departament::class, 'parent_id');
    }

    /***
     * 
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Departament::class, 'parent_id');
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $s
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $s)
    {
        if($s){
            return $query->where('name', 'like', "%{$s}%");
        }
    }
}
