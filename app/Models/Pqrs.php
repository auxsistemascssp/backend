<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pqrs extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_name',
        'pqrs_type_id',
        'departament_id',
        'pqrs_state_id',
        'email',
        'description',
        'response'
    ];

    /**
     * 
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(PqrsType::class, 'pqrs_type_id');
    }

    /**
     * 
     */
    public function departament()
    {
        return $this->belongsTo(Departament::class, 'departament_id');
    }

    /**
     * 
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(PqrsState::class, 'pqrs_state_id');
    }
}
