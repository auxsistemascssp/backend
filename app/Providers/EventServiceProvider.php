<?php

namespace App\Providers;

use App\Events\EmployeeUpdated;
use App\Events\PqrsSended;
use App\Events\PqrsUpdateEvent;
use App\Listeners\EmployeeUpdatedListener;
use App\Listeners\PqrsUpdateListener;
use App\Listeners\SendEmailPqrsSendedNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        PqrsSended::class => [
            SendEmailPqrsSendedNotification::class
        ],
        PqrsUpdateEvent::class => [
            PqrsUpdateListener::class
        ],
        EmployeeUpdated::class => [
            EmployeeUpdatedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
