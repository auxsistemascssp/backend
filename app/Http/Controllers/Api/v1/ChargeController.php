<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\ChargeCreateRequest;
use App\Http\Requests\ChargeUpdateRequest;
use App\Models\Charge;
use Illuminate\Http\Request;

class ChargeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $charges = Charge::search($request->q)
        ->with(['phones', 'departament'])
        ->orderBy('name');


        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($charges->paginate($limit));

        return $this->successResponse($charges->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChargeCreateRequest $request)
    {
        $charge = Charge::create($request->all());

        return $this->successResponse($charge);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Charge  $charge
     * @return \Illuminate\Http\Response
     */
    public function show(Charge $charge)
    {
        $charge->phones;
        return $this->successResponse($charge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Charge  $charge
     * @return \Illuminate\Http\Response
     */
    public function update(ChargeUpdateRequest $request, Charge $charge)
    {
        $charge->fill($request->all());
        $charge->update();

        return $this->successResponse($charge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Charge  $charge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Charge $charge)
    {
        $charge->delete();
        
        return $this->successResponse([], 204);
    }
}
