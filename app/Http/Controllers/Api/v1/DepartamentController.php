<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\DepartamentCreateRequest;
use App\Http\Requests\DepartamentUpdateRequest;
use App\Models\Departament;
use Illuminate\Http\Request;

class DepartamentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $departaments = Departament::search($request->q)
        ->with('parent')
        ->orderBy('name', 'asc');

        if(isset($request->paginate) && $request->paginate == true){  
            return $this->successResponse($departaments->paginate($limit));
        }

        return $this->successResponse($departaments->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartamentCreateRequest $request)
    {
        $departament = Departament::create($request->all());

        return $this->successResponse($departament);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Departament  $departament
     * @return \Illuminate\Http\Response
     */
    public function show(Departament $departament)
    {
        return $this->successResponse($departament);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departament  $departament
     * @return \Illuminate\Http\Response
     */
    public function update(DepartamentUpdateRequest $request, Departament $departament)
    {
        $departament->fill($request->all());
        $departament->update();

        return $this->successResponse($departament);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Departament  $departament
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departament $departament)
    {
        $departament->delete();

        return $this->successResponse(['success'=>true]);
    }
}
