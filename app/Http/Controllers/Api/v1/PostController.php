<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\AdCreateRequest;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\EventCreateRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $limit = (isset($request->limit)) ? $request->limit : 10;

        $posts = Post::search($request->q)
        ->with(['type', 'state', 'autor'])
        ->type($request->type_id)
        ->state($request->state_id);

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($posts->orderBy('id', 'desc')->paginate($limit));

        return $this->successResponse($posts->orderBy('id', 'desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        if($request->has('cover')) {

            $post = new Post($request->all());
            
            if($post->loadCover($request->cover, 'posts', "cover")) {
                $post->user_id = 1;
                $post->post_type_id = 1;
                $post->post_state_id = 1; 

                $post->save();
                return $this->successResponse($post);
            }
        }
    }

    public function storeEvent(EventCreateRequest $request)
    {
        if($request->has('cover')) {
            
            $event = new Post($request->all());

            if($event->loadCover($request->cover, 'posts', "cover")){
                $event->post_type_id = 3;
                $event->user_id = 1;

                $event->save();

                return $this->successResponse($event);
            }
        }
        
    }

    public function storeAd(AdCreateRequest $request)
    {
        if($request->has('cover')) {
            
            $event = new Post($request->all());

            if($event->loadCover($request->cover, 'posts', "cover")){
                $event->post_state_id = 1;
                $event->user_id = 1;

                $event->save();

                return $this->successResponse($event);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->state;
        $post->type;
        $post->autor;

        return $this->successResponse($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, Post $post)
    {
        $post->fill($request->all());
        $post->update();

        return $this->successResponse($post);
    }

    /***
     * 
     */
    public function updateEvent(EventUpdateRequest $request, Post $event)
    {
        $event->fill(($request->all()));
        $event->update();

        return $this->successResponse($event); 
    }


    /**
     * 
     */
    public function updateCover(Request $request, Post $post)
    {
        if($request->has('cover') && $post->loadCover($request->cover, 'posts', "cover")){

            $post->update();

            return $this->successResponse($post);
        }

        return $this->errorResponse([
            'cover' =>  ["No se pudo cargar la imagen, vuelve a intentarlo"]
        ], 'Error inesperado', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return $this->successResponse([], 204);
    }


    /**
     * 
     */
    public function deleteCover(Request $request, Post $post)
    {
        $pathFile = str_replace(env('APP_URL').'/', "", $post->cover);
        
        Storage::delete('public/'.$pathFile);
        
        $post->cover = '';
        $post->update();
        

        return $this->successResponse($post);
    }
}
