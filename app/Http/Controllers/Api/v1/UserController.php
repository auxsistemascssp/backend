<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\UserChangePassword;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends ApiController
{
    /**
     * 
     */
    public function __construct()
    {
        // $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $users = User::search($request->q)
        ->whereHas('roles')->with('roles');

        if(isset($request->paginate) && $request->paginate == true){
            return $this->successResponse($users->paginate($limit));
        }


        return $this->successResponse($users->get());
    }

    /**
     * 
     */
    public function getClientIp(Request $request)
    {
        return $this->successResponse(request()->ip());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        DB::transaction(function() use ($request) {

            $user = new User($request->all());
            $user->password = bcrypt($request->password);
            
            $user->save();
            $user->assignRole($request->role);
            
            return $this->successResponse($user);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $rol = $user->roles()->first();

        return $this->successResponse([
            'user'  =>  $user,
            'rol'   =>  $rol
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {   
        DB::transaction(function() use ($request, $user) {

            $user->fill($request->all());
            $user->assignRole($request->role);
            $user->update();
            
            return $this->successResponse($user);   
        });

    }

    /**
     * 
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(UserChangePassword $request, User $user)
    {
        $user->password = bcrypt($request->password);
        $user->update();

        return $this->successResponse([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return $this->successResponse([], 204);
    }

    /**
     * 
     */
    public function updatePhoto(Request $request, User $user)
    {
        if(
            $request->has('cover') && 
            $user->loadCover(
                $request->cover, 
                "users", 
                "cover", 
                $user->identification_number
            ) 
        ){
            $user->update();
            return $this->successResponse([], 204);
        }

        return $this->errorResponse([
            'cover' =>  ["No se pudo cargar la imagen, vuelve a intentarlo"]
        ], 'Error inesperado', 500);
    }

    /**
     * 
     */
    public function deletePhoto(Request $request, User $user)
    {
        if($user->deleteCover('cover')){
            
            $user->update();
            return $this->successResponse([], 204);
        }

        return $this->errorResponse([
            'cover' =>  ["No se pudo eliminar la imagen, vuelve a intentarlo"]
        ], 'Error inesperado', 500);
    }
}
