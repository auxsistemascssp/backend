<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::query();
        
        $limit = (isset($request->limit)) ? $request->limit : 6;
        
        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($roles->orderBy('id', 'desc')->paginate($limit));

        return $this->successResponse($roles->orderBy('id', 'desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleCreateRequest $request)
    {
        $role = Role::create([
            'name'      =>  $request->name
        ]); 
        return $this->successResponse($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Spatie\Permission\Models\Role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $role->permissions;
        return $this->successResponse($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Spatie\Permission\Models\Role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, Role $role)
    {
        $role->fill($request->all());
        $role->update();
        
        return $this->successResponse($role);
    }
    
    /**
     * 
     */
    public function updatePermissions(Request $request, Role $role)
    {
        if(isset($request->action) && $request->action === 'add'){
            $role->givePermissionTo($request->permission_id);
        }else if(isset($request->action) && $request->action === 'remove'){
            $role->revokePermissionTo($request->permission_id);
        }

        return $this->successResponse($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Spatie\Permission\Models\Role
     * @return \Illuminate\Http\Response
     */

    public function destroy(Role $role)
    {
        $role->delete();

        return $this->successResponse([], 204);
    }
}
