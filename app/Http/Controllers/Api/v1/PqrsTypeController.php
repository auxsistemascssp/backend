<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\PqrsType;
use Illuminate\Http\Request;

class PqrsTypeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = PqrsType::all();

        return $this->successResponse($types, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PqrsType  $pqrsType
     * @return \Illuminate\Http\Response
     */
    public function show(PqrsType $pqrsType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PqrsType  $pqrsType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PqrsType $pqrsType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PqrsType  $pqrsType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PqrsType $pqrsType)
    {
        //
    }
}
