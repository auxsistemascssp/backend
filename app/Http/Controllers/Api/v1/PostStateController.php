<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\PostState;
use Illuminate\Http\Request;

class PostStateController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postStates = PostState::all();

        return $this->successResponse($postStates);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostState  $postState
     * @return \Illuminate\Http\Response
     */
    public function show(PostState $postState)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PostState  $postState
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostState $postState)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PostState  $postState
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostState $postState)
    {
        //
    }
}
