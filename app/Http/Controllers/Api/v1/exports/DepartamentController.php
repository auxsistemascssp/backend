<?php

namespace App\Http\Controllers\Api\v1\exports;

use App\Exports\Departament\UploadTemplate as DepartamentUploadTemplate;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class DepartamentController extends ApiController
{
    public function exportUploadTemplate()
    {
        Excel::store(new DepartamentUploadTemplate, 'departamentos.xlsx', 'public');

        $filename = 'departamentos.xlsx';

        $url = Storage::disk('public')->url($filename);
        
        return $this->successResponse([
            'url' =>    $url
        ]);
    }
}
