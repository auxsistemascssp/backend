<?php

namespace App\Http\Controllers\Api\v1\exports;

use App\Exports\Employee\EmployeeUploadTemplate;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends ApiController
{
    public function exportUploadTemplate()
    {
        Excel::store(new EmployeeUploadTemplate, 'empleados.xlsx', 'public');

        $filename = 'empleados.xlsx';

        $url = Storage::disk('public')->url($filename);
        
        return $this->successResponse([
            'url' =>    $url
        ]);
    }
}
