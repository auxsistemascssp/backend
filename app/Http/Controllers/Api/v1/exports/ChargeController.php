<?php

namespace App\Http\Controllers\Api\v1\exports;

use App\Exports\Charge\ChargeUplodaTemplate;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ChargeController extends ApiController
{
    public function exportUploadTemplate(Request $request)
    {
        Excel::store(new ChargeUplodaTemplate, 'cargos.xlsx', 'public');

        $filename = 'cargos.xlsx';

        $url = Storage::disk('public')->url($filename);
        
        return $this->successResponse([
            'url' =>    $url
        ]);
    }
}
