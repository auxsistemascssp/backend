<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentCreateRequest;
use App\Http\Requests\DocumentUpdateRequest;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $documents = Document::search($request->q)
        ->with('departament', 'process');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($documents->paginate($limit));

        return $this->successResponse($documents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentCreateRequest $request)
    {
        $document = new Document($request->all());

        if($request->has('src') && $document->loadCover($request->src, 'documents', 'src', time())){

            $document->save();

            return $this->successResponse($document);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        $document->departament;
        $document->process;

        return $this->successResponse($document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentUpdateRequest $request, Document $document)
    {
        $document->fill(($request->all()));
        $documentUpdated = $document->update();

        return $this->successResponse($documentUpdated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        if($document->deleteCover("src")){
            $document->delete();

            return $this->successResponse([], 204);
        }
    }

    /**
     * 
     * 
     */
    public function deleteFile(Request $request, Document $document)
    {
        if($document->deleteCover("src")){
            $document->update();

            return $this->successResponse($document);
        }
    }

    /**
     * 
     * 
     */
    public function uploadFile(Request $request, Document $document)
    {
        $request->validate([
            'src'   =>  'required|mimes:pdf,docx,xlsx,pptx'
        ], [
            'src.required'  =>  'El archivo es requerido',
            'src.mimes'     =>  'Solo se permiten archivos con extensiones .pdf, .docx, .xlsx, .pptx'
        ]);

        if($request->hasFile('src') && $document->loadCover($request->src, "documents", "src")){
            $document->update();

            return $this->successResponse($document); 
        }
    }


}
