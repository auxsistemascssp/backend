<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\EmployeeUpdated;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\EmployeeCreateRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $limit = (isset($request->limit)) ? $request->limit : 10;
        $employees = Employee::search($request->q)
        ->with(['user', 'charge.departament']);
        
        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($employees->paginate($limit));

        return $this->successResponse($employees->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeCreateRequest $request)
    {
        DB::transaction(function() use ($request){
            
            $user = new User($request->all());
            
            $fileName = substr(strtoupper($request->name), 0, 1).''.substr(strtoupper($request->last_name), 0, 1);

            if($user->loadCoverDefault("users", $fileName)){

                $user->username = $request->email;
                $user->password = bcrypt('cssp_generica');
                $user->save();
                
                $employee = $user->employee()->save(new Employee($request->all()));
                
                return $this->successResponse($employee);
            }
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $employee->user;
        $employee->charge;

        return $this->successResponse($employee);
    }

    /**
     * 
     */
    public function showBirthdays(Request $request)
    {
        $birthday = Employee::active()
        ->with(['user', 'charge'])
        ->whereRaw('MONTH(birthday) = MONTH(CURRENT_DATE())')
        ->orderByRaw('DAY(birthday)', 'desc')
        ->get();

        return $this->successResponse($birthday);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {   
        $user = User::find($employee->user_id);
        $user->fill($request->all());
        $user->update();
        
        $employee->fill($request->all());
        
        $employee->update();
        $employee->user;
        $employee->charge;

        event(new EmployeeUpdated($employee));

        return $this->successResponse($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $user = $employee->user;    
        
        $user->deleteCover('cover');
        $user->delete();
        return $this->successResponse([], 204);
    }
}
