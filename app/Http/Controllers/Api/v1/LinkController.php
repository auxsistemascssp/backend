<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\LinkCreateRequest;
use App\Http\Requests\LinkUpdateRequest;
use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $links = Link::search($request->q)
        ->with('category')
        ->linked($request->linked)
        ->limit($request->limit);


        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($links->paginate($limit));

        return $this->successResponse($links->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LinkCreateRequest $request)
    {   
        
        $link = new Link($request->all());
        $link->is_linked_home = (isset($request->is_linked_home)) ? true : false;

        if($request->has('cover') && $request->cover != null) {
            $link->loadCover($request->cover, 'links', "cover", time());
        }else {
            $name = strtoupper(substr($request->name, 0, 2));
            $link->loadCoverDefault('links', $name);
        }
        
        $link->save();
        return $this->successResponse($link);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        $link->category;

        return $this->successResponse($link);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(LinkUpdateRequest $request, Link $link)
    {
        $link->fill($request->all());
        $link->is_linked_home = (isset($request->is_linked_home)) ? true : false;

        $link->update();
        return $this->successResponse($link);
    }

    /**
     * 
     */
    public function updateCover(Request $request, Link $link)
    {
        if($request->has('cover') && $link->loadCover($request->cover, 'links', "cover", time())){

            $link->update();

            return $this->successResponse($link);
        }

        return $this->errorResponse([
            'cover' =>  ["No se pudo cargar la imagen, vuelve a intentarlo"]
        ], 'Error inesperado', 500);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        $link->delete();

        return $this->successResponse([], 204);
    }

    /**
     * 
     */
    public function deleteCover(Link $link)
    {
        $link->deleteCover("cover");
        $link->update();

        return $this->successResponse($link);
    }
}
