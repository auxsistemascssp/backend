<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\PhotoAlbumCreateRequest;
use App\Http\Requests\PhotoAlbumUpdateRequest;
use App\Http\Requests\PhotoUploadRequest;
use App\Models\Photo;
use App\Models\PhotoAlbum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoAlbumController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 6;

        $albums = PhotoAlbum::search($request->q)
        ->orderBy('id', 'desc');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($albums->paginate($limit));

        return $this->successResponse($albums->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoAlbumCreateRequest $request)
    {

        $photoAlbum = PhotoAlbum::create($request->all());

        return $this->successResponse($photoAlbum);
    }

    /**
     * 
     */
    public function storePhoto(PhotoUploadRequest $request, PhotoAlbum $photoAlbum)
    {   
        if($request->has('photos')){
            foreach($request->file('photos') as $key => $photo){
                
                $photoModel = new Photo();

                $name = time().$key;

                if($photoModel->loadCover($photo, 'photos', "cover", $name)){
                    $photoAlbum->photos()->save($photoModel);
                }
            }
        }

        $photos = $photoAlbum->photos;

        return $this->successResponse($photos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PhotoAlbum  $photoAlbum
     * @return \Illuminate\Http\Response
     */
    public function show(PhotoAlbum $photoAlbum)
    {
        $photoAlbum->photos;

        return $this->successResponse($photoAlbum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PhotoAlbum  $photoAlbum
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoAlbumUpdateRequest $request, PhotoAlbum $photoAlbum)
    {
        $photoAlbum->fill($request->all());
        $photoAlbum->update();

        return $this->successResponse($photoAlbum);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PhotoAlbum  $photoAlbum
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhotoAlbum $photoAlbum)
    {
        $photoAlbum->delete();
        
        return $this->successResponse([], 204);
    }

    /**
     * 
     */
    public function destroyPhoto(PhotoAlbum $photoAlbum, Photo $photo)
    {
        if($photo->deleteCover("cover")){
            $photo->delete();

            return $this->successResponse($photoAlbum->photos);
        }

        return $this->errorResponse(
            [], 
            'No se pudo eliminar la foto, ocurrio un error', 
            500
        );
    }
}

