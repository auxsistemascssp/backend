<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarouselItemCreateRequest;
use App\Http\Requests\UpdateCarouselRequest;
use App\Models\CarouselItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CarouselItemController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = CarouselItem::all();

        return $this->successResponse($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarouselItemCreateRequest $request)
    {
        $item = new CarouselItem($request->all());

        if($request->has('cover') && $item->loadCover($request->cover,'slides', "cover")){
            
            if($request->hasFile('src') && !$item->loadCover($request->src, 'uploads', "src")){
                return $this->errorResponse([
                    'resource_type_id' => ['No se pudo cargar el recurso']
                ], "Error al cargar el recurso", 422);
            }

            $item->save();
            return $this->successResponse($item);
        }

        return $this->errorResponse([
            'cover' =>  ["No se pudo cargar la imagen, vuelve a intentarlo"]
        ], 'Error inesperado', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CarouselItem  $carouselItem
     * @return \Illuminate\Http\Response
     */
    public function show(CarouselItem $carouselItem)
    {
        $carouselItem->resource;

        return $this->successResponse($carouselItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CarouselItem  $carouselItem
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarouselRequest $request, CarouselItem $carouselItem)
    {
        $carouselItem->fill($request->all());
        $carouselItem->src = (isset($request->src) && $request->src !== null) ? $request->src : $carouselItem->getOriginal('src');

        $carouselItem->update();

        return $carouselItem;
    }


    /**
     * 
     */
    public function updateCover(Request $request, CarouselItem $carouselItem)
    {
        if($request->has('cover') && $carouselItem->loadCover($request->cover,'slides', "cover")){
            $carouselItem->update();

            return $this->successResponse($carouselItem);
        }

        return $this->errorResponse(
            [], 
            'No se pudo actualizar el cover, ocurrio un error', 
            500
        );
    }

    /**
     * 
     */
    public function updateResource(Request $request, CarouselItem $carouselItem)
    {
        if($request->hasFile('src') && $carouselItem->loadCover($request->src,'slides', "src")){
            $carouselItem->update();

            return $this->successResponse($carouselItem);
        }

        return $this->errorResponse(
            [], 
            'No se pudo actualizar el recurso, ocurrio un error', 
            500
        );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CarouselItem  $carouselItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarouselItem $carouselItem)
    {
        if($carouselItem->deleteCover("cover")){

            $carouselItem->delete();
            return $this->successResponse([], 204);
        }
        
        return $this->errorResponse(
            [], 
            'No se pudo eliminar el item, ocurrio un error', 
            500
        );
    }

    /*
     *
     * 
     */
    public function deleteCover(CarouselItem $carouselItem)
    {
        if($carouselItem->deleteCover("cover")){
            $carouselItem->update();

            return $this->successResponse($carouselItem);
        }

        return $this->errorResponse(
            [], 
            'No se pudo eliminar el cover, ocurrio un error', 
            500
        );
    }

    /* 
     *
     * 
     */
    public function deleteResource(CarouselItem $carouselItem)
    {
        
        if($carouselItem->deleteCover("src")){
            
        }else {
            $carouselItem->src = "";
        }
        
        $carouselItem->update();
        return $this->successResponse($carouselItem);
        
    }
}
