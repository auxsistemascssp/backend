<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\CategoryPermission;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends ApiController
{
    public function index()
    {
        $permissions = CategoryPermission::whereHas('permissions')
        ->with('permissions.action')
        ->get();

        return $this->successResponse($permissions);
    }
}
