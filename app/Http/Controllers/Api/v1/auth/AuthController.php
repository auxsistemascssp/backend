<?php

namespace App\Http\Controllers\Api\v1\auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMyInfoRequest;
use App\Http\Requests\UpdateMyPasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{

    /**
     * 
     */
    public function me(Request $request)
    {
        return $this->successResponse($request->user());
    }

    /**
     * 
     */
    public function permisssions(Request $request)
    {
        $permissions = $request->user()->roles()
        ->with('permissions')
        ->get()
        ->pluck('permissions')
        ->collapse();

        return $this->successResponse($permissions);
    }
    
    /**
     * 
     */
    public function updateMyInfo(UpdateMyInfoRequest $request)
    {
        $user = $request->user();
        $user->fill($request->all());
        $user->update();

        return $this->successResponse($user);
    }

    /**
     * 
     */
    public function updateMyPassword(UpdateMyPasswordRequest $request)
    {
        if(Hash::check($request->current_password, $request->user()->password)){
            $user = $request->user();
            $user->password = bcrypt($request->password);
            $user->update();
            
            return $this->successResponse($user);
        }else {

            return $this->errorResponse([
                'current_password' => ['La contraseña es incorrecta']
            ], '', 422);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return $this->successResponse([], 204);
    }
}
