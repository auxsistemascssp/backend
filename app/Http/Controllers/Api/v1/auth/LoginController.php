<?php

namespace App\Http\Controllers\Api\v1\auth;

use App\Http\Controllers\Api\ApiController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends ApiController
{

    public function show(Request $request)
    {
        if ($request->expectsJson()) {
            return $this->successResponse(null, 204);
        }

        return $this->successResponse([], 204);
    }

    public function login(Request $request)
    {
        $this->validateRequest($request);

        $credentials = $request->only(['email', 'password']);

        if(Auth::attempt($credentials)){
            $token = $request->user()->createToken($request->user()->email);

            return $this->successResponse([
                'success'   =>  true,
                'user'      =>  $request->user(),
                'token'     =>  $token->plainTextToken
            ], 200);
        }

        return $this->errorResponse([
            'email' =>  'Correo o contraseña invalida'
        ], 'Error de validaciòn' ,422);
    }

    private function validateRequest(Request $request)
    {
        $request->validate([
            'email'     =>  'required|email',
            'password'  =>  'required'
        ],[
            'email.required'    =>  'Ingrese el email',
            'email.email'       =>  'Ingrese un email valido',
            'password.required' =>  'Ingrese la contraseña'
        ]);
    }
}
