<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Process;
use Illuminate\Http\Request;

class ProcessController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $processes = Process::search($request->q)
        ->orderBy('id', 'desc');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($processes->paginate($limit));

        return $this->successResponse($processes->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required|unique:processes'
        ],[
            'name.required' =>  'El nombre es requerido',
            'name.unique' =>  'El proceso ya esta registrado',
        ]);

        $processSaved = Process::create($request->all());

        return $this->successResponse($processSaved);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $process)
    {
        return $this->successResponse($process);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Process $process)
    {
        $request->validate([
            'name'  =>  'required|unique:processes,name,'.$process->id
        ],[
            'name.required' =>  'El nombre es requerido',
            'name.unique'   =>  'El proceso ya esta registrado',
        ]);

        $process->fill($request->all());
        $procesUpdated = $process->update();

        return $this->successResponse($procesUpdated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $process)
    {
        $process->delete();

        return $this->successResponse([], 204);
    }
}
