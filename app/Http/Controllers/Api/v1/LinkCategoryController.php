<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\LinkCategoryCreateRequest;
use App\Http\Requests\LinkCategoryUpdateRequest;
use App\Models\LinkCategory;
use Illuminate\Http\Request;

class LinkCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $categories = LinkCategory::search($request->q)
        ->with('links');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($categories->paginate($limit));

        return $this->successResponse($categories->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LinkCategoryCreateRequest $request)
    {
        $category = LinkCategory::create($request->all());
        
        return $this->successResponse($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LinkCategory  $linkCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LinkCategory $linkCategory)
    {
        return $this->successResponse($linkCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LinkCategory  $linkCategory
     * @return \Illuminate\Http\Response
     */
    public function update(LinkCategoryUpdateRequest $request, LinkCategory $linkCategory)
    {
        $linkCategory->fill($request->all());
        $linkCategory->update();

        return $this->successResponse($linkCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LinkCategory  $linkCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinkCategory $linkCategory)
    {
        $linkCategory->delete();

        return $this->successResponse([], 204);
    }
}
