<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\PhoneDirectoryCreateRequest;
use App\Http\Requests\PhoneDirectoryUpdateRequest;
use App\Models\Charge;
use App\Models\ChargePhone;
use Illuminate\Http\Request;

class PhoneDirectoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $phones = ChargePhone::search($request->q)
        ->orderBy('name', 'asc');

        if(isset($request->paginate) && $request->paginate == true){
             
            return $this->successResponse($phones->paginate($limit));
        }

        return $this->successResponse($phones->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhoneDirectoryCreateRequest $request)
    {   
        $phone = new ChargePhone($request->all());
        
        if(is_null($request->name)){
            $charge = Charge::find($request->charge_id);
            $phone->name = $charge->name;
        }

        $phone->save();

        return $this->successResponse($phone);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChargePhone  $chargePhone
     * @return \Illuminate\Http\Response
     */
    public function show(ChargePhone $chargePhone)
    {
        return $this->successResponse($chargePhone);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChargePhone  $chargePhone
     * @return \Illuminate\Http\Response
     */
    public function update(PhoneDirectoryUpdateRequest $request, ChargePhone $chargePhone)
    {
        $chargePhone->fill($request->all());
        $chargePhone->update();

        return $this->successResponse($chargePhone);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChargePhone  $chargePhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChargePhone $chargePhone)
    {
        $chargePhone->delete();
        
        return $this->successResponse([], 204);
    }
}
