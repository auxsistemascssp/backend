<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $limit = (isset($request->limit)) ? $request->limit : 6;

        $posts = Post::with(['type', 'state', 'autor'])
        ->type($request->type_id)
        ->title($request->q)
        ->state($request->state_id);

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($posts->orderBy('id', 'desc')->paginate($limit));

        return $this->successResponse($posts->orderBy('id', 'desc')->get());
    }

    /**
     * 
     */
    public function show(Post $post)
    {
        return $this->successResponse($post);
    }
    
    /**
     * 
     */
    public function lastestPosts(Request $request)
    {
               
        $posts = Post::with('autor')
        ->type($request->type_id)
        ->orderBy('id', 'desc')
        ->limit(6)
        ->get();

        return $this->successResponse($posts);
    }

    /**
     * 
     */
    public function getPopup(Request $request){

        
        $popUp = Post::where([
            ['post_type_id', '=', 4],
            ['post_state_id', '=', 1]
        ])
        ->orderBy('id', 'desc')
        ->first();
        
        return $this->successResponse($popUp);
    }
}
