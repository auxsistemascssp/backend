<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\PhotoAlbum;
use Illuminate\Http\Request;

class PhotoAlbumController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 6;
        
        $albums = PhotoAlbum::with('photos')
        ->name($request->name);
        
        if(isset($request->paginate) && $request->paginate == true)
        return $this->successResponse($albums->orderBy('id', 'desc')->paginate($limit));
        
        return $this->successResponse($albums->orderBy('id', 'desc')->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PhotoAlbum  $photoAlbum
     * @return \Illuminate\Http\Response
     */
    public function show(PhotoAlbum $photoAlbum)
    {
        $photoAlbum->photos;

        return $this->successResponse($photoAlbum);
    }
}
