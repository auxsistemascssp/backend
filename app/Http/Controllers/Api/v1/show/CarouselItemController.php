<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Models\CarouselItem;
use Illuminate\Http\Request;

class CarouselItemController extends ApiController
{
    public function index(Request $request)
    {
        $items = CarouselItem::orderBy('id', 'desc')
        ->get();

        return $this->successResponse($items);
    }
}
