<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\PostType;
use App\Models\PqrsType;
use Illuminate\Http\Request;

class PostTypeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $types = PqrsType::all();

        return $this->successResponse($types, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostType  $postType
     * @return \Illuminate\Http\Response
     */
    public function show(PostType $postType)
    {
        //
    }
}
