<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 6;
        
        $albums = Video::query()
        ->name($request->name);
        
        if(isset($request->paginate) && $request->paginate == true)
        return $this->successResponse($albums->orderBy('id', 'desc')->paginate($limit));
        
        return $this->successResponse($albums->orderBy('id', 'desc')->get());
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return $this->successResponse($video);
    }
}
