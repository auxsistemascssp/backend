<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Models\Charge;
use App\Models\ChargePhone;
use Illuminate\Http\Request;

class PhoneDirectoryController extends ApiController
{
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $charges = ChargePhone::search($request->q)
        ->with(['charge.departament'])
        ->orderBy('name', 'asc');


        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($charges->paginate($limit));

        return $this->successResponse($charges->get());
    }
}
