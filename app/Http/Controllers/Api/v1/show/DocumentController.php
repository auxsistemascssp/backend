<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends ApiController
{
    public function index(Request $request)
    {

        $documents = Document::search($request->q)
        ->with('departament', 'process');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($documents->paginate(10));

        return $this->successResponse($documents->get());
    }
}
