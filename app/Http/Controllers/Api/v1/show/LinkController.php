<?php

namespace App\Http\Controllers\Api\v1\show;

use App\Http\Controllers\Api\ApiController;
use App\Models\Link;
use App\Models\LinkCategory;
use Illuminate\Http\Request;

class LinkController extends ApiController
{
    public function index(Request $request)
    {
        $links = Link::with('category')
        ->linked($request->linked)
        ->limit($request->limit);


        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($links->paginate(5));

        return $this->successResponse($links->get());
    }

    public function search(Request $request)
    {
        $links = LinkCategory::with(['links' => function($query) use ($request){
            $query->where('name', 'like', "%{$request->q}%");
        }])
        ->whereHas('links')
        ->get();

        return $this->successResponse($links);
    }
}
