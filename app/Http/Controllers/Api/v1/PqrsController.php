<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\PqrsSended;
use App\Events\PqrsUpdateEvent;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\PqrsCreateRequest;
use App\Models\Pqrs;
use Illuminate\Http\Request;

class PqrsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $pqrs = Pqrs::with(['type', 'departament', 'state']);

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($pqrs->orderBy('id', 'desc')->paginate($limit));

        return $this->successResponse($pqrs->orderBy('id', 'desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PqrsCreateRequest $request)
    {
        $pqrs = Pqrs::create($request->all());
        $pqrs->state;
        $pqrs->type;
        $pqrs->departament;
        
        // Dispatch Event
        event(new PqrsSended($pqrs));

        return $this->successResponse($pqrs, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pqrs  $pqr
     * @return \Illuminate\Http\Response
     */
    public function show(Pqrs $pqr)
    {
        $pqr->state;
        $pqr->departament;
        $pqr->type;

        return $this->successResponse($pqr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pqrs  $pqr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pqrs $pqr)
    {
        $pqr->fill($request->all());
        
        
        if($pqr->getOriginal('pqrs_state_id') !== $request->pqrs_state_id){
            // Fire Event
            event(new PqrsUpdateEvent($pqr));
        }
        
        $pqr->update();

        $pqr->state;
        $pqr->departament;
        $pqr->type;
        
        return $this->successResponse($pqr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pqrs  $pqr
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pqrs $pqr)
    {
        $pqr->delete();

        return $this->successResponse([], 204);
    }
}
