<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\DocumentType;
use Illuminate\Http\Request;

class DocumentTypeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dTypes = DocumentType::orderBy('id', 'desc');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($dTypes->paginate(10));

        return $this->successResponse($dTypes->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required|unique:document_types'
        ],[
            'name.required' =>  'El nombre es requerido'
        ]);

        $type = DocumentType::create($request->all());
        
        return $this->successResponse($type);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentType $documentType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentType $documentType)
    {
        $request->validate([
            'name'  =>  'required|unique:document_types,id,'.$documentType->id
        ],[
            'name.required' =>  'El nombre es requerido'
        ]);

        $documentType->fill($request->all());
        $documentType->update();

        return $this->successResponse($documentType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentType $documentType)
    {
        $documentType->delete();

        return $this->successResponse([], 204);
    }
}
