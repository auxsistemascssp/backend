<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoCreateRequest;
use App\Http\Requests\VideoUpdateRequest;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (isset($request->limit)) ? $request->limit : 10;

        $videos = Video::search($request->q)
        ->name($request->q)
        ->orderBy('id', 'desc');

        if(isset($request->paginate) && $request->paginate == true)
            return $this->successResponse($videos->paginate($limit));

        return $this->successResponse($videos->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoCreateRequest $request)
    {
        if($request->has('video')){
            $videoModel = new Video($request->all());
            $videoModel->src = $videoModel->loadFile($request->video, "videos");
            $videoModel->save();

            return $this->successResponse($videoModel);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return $this->successResponse($video);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(VideoUpdateRequest $request, Video $video)
    {
        $video->fill($request->all());
        $video->update();

        return $this->successResponse($video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        if($video->removeFile($video->src)){
            $video->delete();            
            return $this->successResponse($video);
        }
    }

    /**
     * 
     */
    public function deleteVideoMedia(Video $video)
    {
        if($video->removeFile($video->src)){
            $video->update([
                'src' => ''
            ]);
            
            return $this->successResponse($video);
        }
    }

    /**
     * 
     */
    public function uploadVideoMedia(Request $request, Video $video)
    {
        $request->validate([
            'video' =>  'required|mimes:mp4,avi'
        ],[
            'video.required'    =>  'El viedeo es requerido',
            'video.mimes'       =>  'Solo se aceptan videos con exntesiones .mp4 y .avi'
        ]);

        if($request->has('video')){
            $video->src = $video->loadFile($request->video, "videos");
            $video->update();

            return $this->successResponse($video);
        }
    }
}
