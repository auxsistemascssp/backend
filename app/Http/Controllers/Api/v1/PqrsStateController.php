<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\PqrsState;
use Illuminate\Http\Request;

class PqrsStateController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postStates = PqrsState::all();

        return $this->successResponse($postStates);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PqrsState  $pqrsState
     * @return \Illuminate\Http\Response
     */
    public function show(PqrsState $pqrsState)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PqrsState  $pqrsState
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PqrsState $pqrsState)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PqrsState  $pqrsState
     * @return \Illuminate\Http\Response
     */
    public function destroy(PqrsState $pqrsState)
    {
        //
    }
}
