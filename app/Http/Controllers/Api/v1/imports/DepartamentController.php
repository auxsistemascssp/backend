<?php

namespace App\Http\Controllers\Api\v1\imports;

use App\Http\Controllers\Api\ApiController;
use App\Imports\Departament\DepartamentImport;
use Illuminate\Http\Request;

class DepartamentController extends ApiController
{
    public function uploadTemplate(Request $request) 
    {
        $import = new DepartamentImport();
        $import->import($request->file);

        return $this->successResponse([], 204);
    }
}
