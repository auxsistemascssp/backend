<?php

namespace App\Http\Controllers\Api\v1\imports;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Imports\Charge\ChargeTemplate;
use Illuminate\Http\Request;

class ChargeController extends ApiController
{
    public function uploadTemplate(Request $request)
    {
        if($request->hasFile('file')){

            $import = new ChargeTemplate();
            
            $import->import($request->file);

            return $this->successResponse([], 204);
        }
    }
}
