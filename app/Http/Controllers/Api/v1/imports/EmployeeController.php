<?php

namespace App\Http\Controllers\Api\v1\imports;

use App\Http\Controllers\Api\ApiController;
use App\Imports\Employee\EmployeeTemplate;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;


class EmployeeController extends ApiController
{
    public function uploadTemplate(Request $request)
    {
        
        $import = new EmployeeTemplate();
        $import->import($request->file);

    }
}
