<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             =>  'required|unique:posts,title,'.$this->event->id,
            'body'              =>  'required',
            'post_state_id'     =>  'required',
            'start_at'          =>  'required',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'title.required'             =>  'El titulo es requerido',
            'title.unique'               =>  'El titulo ya esta registrado',
            'body.required'              =>  'La descripciòn es requerida',
            'post_state_id.required'     =>  'El estado es requerido',
            'start_at.required'          =>  'La fecha del evento es requerida',       
        ];
    }
}
