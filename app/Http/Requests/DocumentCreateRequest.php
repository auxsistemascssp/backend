<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'              =>  'required',
            'name'              =>  'required',
            'src'               =>  'required|mimes:pdf,docx,xlsx,pptx',
            'document_type_id'  =>  'required',
            'departament_id'    =>  'required',
            'process_id'        =>  'required',
            'version'           =>  'required',
            'origin'            =>  'required',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'code.required'             => 'El código es requerdo',
            'name.required'             => 'El nombre es requerido',
            'src.required'              => 'El archivo es requerido',
            'src.mimes'                 => 'Solo se permiten archivos con extensiones .pdf, .docx, .xlsx, .pptx',
            'document_type_id.required' => 'El tipo de documneto es requerido',
            'departament_id.required'   => 'El responsable es requerido',
            'process_id.required'       => 'El proceso es requerido',
        ];
    }
}
