<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             =>  'required',
            'body'              =>  'required',
            'post_state_id'     =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'            =>  'El titulo es requerido',
            'body.required'             =>  'El contenido es requerido',
            'post_state_id.required'    =>  'El estado es requerido'
        ];
    }
}
