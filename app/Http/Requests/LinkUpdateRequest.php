<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinkUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|unique:links,name,'.$this->link->id,
            'cover'             =>  'mimes:jpeg,png,jpg',
            'link_category_id'  =>  'required',
            'to'                =>  'required|url',
            'target'            =>  'required'
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'             =>  'El nombre es requerido',
            'name.unique'               =>  'El nombre del vinculo ya esta registrado',
            'link_category_id.required' =>  'La categoria es requerida',
            'cover.mimes'               =>  'Solo se acepta imagen de tipo jpeg, jpg y png',
            'to.required'               =>  'El enlace es requerido',
            'to.url'                    =>  'Ingrese una url valida',
            'target.required'           =>  'El modo de apertura es requerido',
        ];
    }
}
