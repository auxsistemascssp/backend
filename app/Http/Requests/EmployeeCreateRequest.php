<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      =>  'required', 
            'last_name'                 =>  'required',
            'email'                     =>  'required|email|unique:users',
            'charge_id'                 =>  'required',
            'identification_number'     =>  'required',
            'birthday'                  =>  'required'
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'                         =>  'El nombre es requerido', 
            'last_name.required'                    =>  'El apellidos es requerido',
            'email.required'                        =>  'El email es requerido',
            'email.email'                           =>  'El ingrese un email valido',
            'email.unique'                          =>  'El email ya esta registrado',
            'charge_id.required'                    =>  'El cargo es requerido',
            'identification_number.required'        =>  'El núemero de identificación es requerido',
            'birthday.required'                     =>  'La fecha de nacimiento es requerido'
        ];
    }
}
