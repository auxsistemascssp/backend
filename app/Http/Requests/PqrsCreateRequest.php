<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PqrsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name'         =>  'required',
            'pqrs_type_id'      =>  'required',
            'departament_id'    =>  'required',
            'email'             =>  'required|email',
            'description'       =>  'required'
        ];
    }

    /**
     *
     * 
     */
    public function messages()
    {
        return [
            'user_name.required'         =>  'El nombre esrequerido',
            'pqrs_type_id.required'      =>  'El çtipo es requerido',
            'departament_id.required'    =>  'El area esrequerida',
            'email.required'             =>  'El correo es requerido',
            'email.email'                =>  'Ingrese un email valido',
            'description.required'       =>  'La descrión es requerida'
        ];
    }
}
