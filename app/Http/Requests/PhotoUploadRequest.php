<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photos.*' =>  'mimes:jpeg,png,jpg'
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'photos.*.mimes'   =>  'Solo se aceptan imagenes con extensiones .jpeg, png, jpg'
        ];
    }
}
