<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarouselItemCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required',
            'cover'     =>  'required|mimes:jpeg,png,jpg',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'     =>  'El titulo es requerida',
            'cover.required'    =>  'La imagen es requerida',
            'cover.mimes'       =>  'Solo se aceptan imagenes con formatos .jpg, jpeg y png',
            'cover.size'        =>  'El archivo es muy pesado'
        ];
    }
}
