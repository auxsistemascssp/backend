<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChargeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|unique:charges',
            'departament_id'    =>  'required'
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'             =>  'El nombre del cargo es requerido',
            'name.unique'               =>  'El cargo ya esta registrado',
            'departament_id.required'   =>  'El departamento es requerido',
        ];
    }
}
