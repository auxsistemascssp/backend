<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|unique:videos',
            'video' =>  'required|mimes:mp4,avi'
        ];
    }


    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'     =>  'El nombre del video es requerido',
            'name.unique'       =>  'El nombre del video ya esta registrado',
            'video.required'    =>  'El viedeo es requerido',
            'video.mimes'       =>  'Solo se aceptan videos con exntesiones .mp4 y .avi'
        ];
    }
}
