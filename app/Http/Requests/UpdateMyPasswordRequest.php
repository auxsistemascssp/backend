<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMyPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password'  =>  'required',
            'password'          =>  'required|confirmed',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'current_password.required'     =>  'La contraseña actual es requerida',
            'password.required'             =>  'La nueva contraseña es requerida',
            'password.current_pasasword'    =>  'La contraseña actual es incorrecta',
            'password.confirmed'            =>  'Las contraseñas no coinciden',
        ];
    }
}
