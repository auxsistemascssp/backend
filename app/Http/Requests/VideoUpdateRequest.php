<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|unique:videos,name,'.$this->video->id
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'     =>  'El nombre del video es requerido',
            'name.unique'       =>  'El nombre del video ya esta registrado'
        ];
    }
}
