<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneDirectoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required',
            'ext'       =>  'required',
            'charge_id' =>  'required',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required' =>  'El nombre es requerido',
            'ext.required'  =>  'La extensión es requerida',
            'ext.unique'    =>  'La extensión la ya esta en uso',
            'charge_id'     =>  'El cargo es requerido'
        ];
    }
}
