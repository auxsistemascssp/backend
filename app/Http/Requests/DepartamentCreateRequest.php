<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartamentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|unique:departaments',            
        ];  
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required' =>  'El nombre del departamento es requerido',
            'name.unique'   =>  'El nombre del departamento ya esta registrado'
        ];
    }
}
