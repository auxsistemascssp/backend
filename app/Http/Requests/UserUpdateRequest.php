<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required',
            'last_name' =>  'required',
            'email'     =>  'required|email|unique:users,email,'.$this->user->id,
            'username'  =>  'required|unique:users,username,'.$this->user->id,
            'role'      =>  'required',
        ];
    }

    /**
     * 
     */
    public function messages()
    {
        return [
            'name.required'         =>  'El nombre es requerido',    
            'last_name.required'    =>  'El apellido es requerido',    
            'email.required'        =>  'El correo es requerido',    
            'email.required'        =>  'El correo no es valido',    
            'email.unique'          =>  'El correo ya esta registrado',    
            'username.required'     =>  'El nombre de usuario es requerido',    
            'username.unique'       =>  'El nombre de usuario ya esta registrado',    
            'role.required'         =>  'El rol es requerido'  
        ];
    }
}
