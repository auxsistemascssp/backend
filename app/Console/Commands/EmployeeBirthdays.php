<?php

namespace App\Console\Commands;

use App\Mail\BirthdayMail;
use App\Models\Employee;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EmployeeBirthdays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commnad for sends emails to employees when is their brithday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $employees = Employee::active()
        ->with(['user', 'charge'])
        ->whereRaw('MONTH(birthday) = MONTH(CURRENT_DATE()) AND DAY(birthday) = DAY(CURRENT_DATE())')
        ->orderByRaw('DAY(birthday)', 'desc')
        ->get();
        
        foreach($employees as $employee){

            Mail::to($employee->user->email)
            ->send(new BirthdayMail($employee));
        }
    }
}