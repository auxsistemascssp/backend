<?php

namespace App\Mail\pqrs;

use App\Models\Pqrs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PqrsUpdateMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     *
     * @var App\Models\Pqrs 
     */
    public $pqrs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pqrs $pqrs)
    {
        $this->pqrs = $pqrs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Tu solicitud de PQRS ha cambiado")
        ->view('emails.pqrs.update')
        ->with('pqrs', $this->pqrs);
    }
}
