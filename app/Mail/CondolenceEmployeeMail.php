<?php

namespace App\Mail;

use App\Models\Employee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CondolenceEmployeeMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * 
     * @var \App\Models\Employee
     */
    public $employee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.condolenceEmployee2')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME', 'INTRANET CSSP'))
        ->subject("Lamentamos el fallecimiento de {$this->employee->user->name} 🖤 ✝");
    }
}
