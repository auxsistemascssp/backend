<?php

namespace App\Mail;

use App\Models\Pqrs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PqrsSendedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * 
     */
    public $pqrs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pqrs $pqrs)
    {
        $this->pqrs = $pqrs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("PQRS")
        ->view('emails.pqrsSended')
        ->with('pqrs', $this->pqrs);
    }
}
