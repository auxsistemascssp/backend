<?php

namespace App\Mail;

use App\Models\Employee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BirthdayMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     *  Employee
     * 
     * @var \App\Models\Employee $employee 
     */
    public $employee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.birthday')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME', 'INTRANET CSSP'))
        ->subject("🎊️ Feliz cumpleaños  {$this->employee->user->name} 🎉️");
    }
}
