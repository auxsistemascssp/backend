<?php

namespace App\Exports\Employee;

use App\Exports\Charge\ChargeExport;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class EmployeeUploadTemplate implements WithHeadings, WithMultipleSheets, WithTitle, WithColumnFormatting
{
    use Exportable;
    
    /**
    * @return array
    */
    public function headings(): array
    {
        return [
            'nombres',
            'apellidos',
            'número de identificación',
            'correo',
            'telefono',
            'cargo',
            'fecha_nacimiento (AAAA-MM-DD)',
        ];    
    }

    /**
     * 
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[1] =  $this;
        $sheets[2] = new ChargeExport();

        return $sheets;
    }

    /**
     * 
     */
    public function title(): string
    {
        return 'Empleados';
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'F' =>  DataType::TYPE_STRING
        ];
    }
}
