<?php

namespace App\Exports\Departament;

use App\Models\Departament;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class DepartamentExport implements FromCollection, WithTitle, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Departament::select('id', 'name', 'parent_id')->get();
    }

    /**
     * 
     */
    public function title(): string
    {
        return 'Departamentos';
    }

    /**
    * @return array
    */
    public function headings(): array
    {
        return [
            'Código',
            'Departamento',
            'Superior',
        ];    
    }
}
