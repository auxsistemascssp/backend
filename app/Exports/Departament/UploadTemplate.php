<?php

namespace App\Exports\Departament;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UploadTemplate implements WithHeadings
{
    use Exportable;
    
    /**
    * @return array
    */
    public function headings(): array
    {
        return [
            'Nombre'
        ];    
    }

    /**
     * 
     */
    public function title(): string
    {
        return 'Departamentos';
    }
}
