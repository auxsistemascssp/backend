<?php

namespace App\Exports\Charge;

use App\Models\Charge;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ChargeExport implements FromCollection, WithTitle, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Charge::select('id', 'name', 'email', 'departament_id')->get();
    } 

    /**
     * 
     */
    public function title(): string
    {
        return 'Cargos';
    }

    /**
    * @return array
    */
    public function headings(): array
    {
        return [
            'Código',
            'Cargo',
            'Correo electrónico',
            'Codigo area',
        ];    
    }
}
