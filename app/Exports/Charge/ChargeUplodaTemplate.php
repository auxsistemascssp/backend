<?php

namespace App\Exports\Charge;

use App\Exports\Departament\DepartamentExport;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ChargeUplodaTemplate implements WithHeadings, WithMultipleSheets
{
    use Exportable;

    /**
    * @return array
    */
    public function headings(): array
    {
        return [
            'Nombre',
            'Email',
            'Codigo departamento'
        ];    
    }

    /**
     * 
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[1] =  $this;
        $sheets[2] = new DepartamentExport();

        return $sheets;
    }
}
