<?php

namespace App\Imports\Employee;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class EmployeeTemplate implements ToModel, WithBatchInserts, WithUpserts, WithMultipleSheets, WithValidation, WithStartRow, WithColumnFormatting
{
    use  Importable;

    /**
    * @param array $row
    *
    */
    public function model(array $row)
    {
        $userFound = User::where('identification_number', '=', $row[2])->first();

        if(!is_null($userFound)){
            return $userFound;
        }

        $user = new User([
            'name'                  =>  $row[0],
            'last_name'             =>  $row[1],
            'identification_number' =>  $row[2],
            'email'                 =>  $row[3],
            'username'              =>  $row[3],
            'password'              =>  bcrypt('cssp_123')
        ]);

        $fileName = substr(strtoupper($user->name), 0, 1).''.substr(strtoupper($user->last_name), 0, 1);

        if($user->loadCoverDefault("users", $user->identification_number, $fileName)){
            $user->save();
            
            return $user->employee()->create(([
                'user_id'   =>  $user->id,
                'phone'     =>  $row[4],
                'charge_id' =>  $row[5],
                'birthday'  =>  Carbon::instance(
                    Date::excelToDateTimeObject($row[6]))
            ]));
        }
    }

    public function batchSize(): int
    {
        return 100;
    }

    public function uniqueBy()
    {
        return 'email';
    }

    /**
     * 
     */
    public function rules(): array
    {
        return [
            '0' =>  'required',
            '1' =>  'required',
            '2' =>  'required|unique:users,email',
            '4' =>  'required'
        ];
    }

    /**
     * 
     */
    public function customValidationMessages(): array
    {
        return [];
    }

    /**
     * 
     */
    public function sheets(): array
    {
        return [
            0 => $this
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'G' =>  NumberFormat::FORMAT_DATE_YYYYMMDD
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
