<?php

namespace App\Imports\Charge;

use App\Models\Charge;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ChargeTemplate implements ToModel, WithValidation, WithStartRow, WithMultipleSheets
{
    use Importable;
    /**
    * @param array $row
    */
    public function model(array $row)
    {
        //
        return new Charge([
            'name'              =>  $row[0],
            'email'             =>  $row[1],
            'departament_id'    =>  $row[2]
        ]);
    }

    /**
     * 
     */
    public function rules(): array
    {
        return [
            '0' =>  'required|unique:charges,name',
            '2' =>  'required',
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '0.required'    =>  'El cargo e requerido',
            '0.unique'      =>  'El nombre ya esta en uso',
            '2.required'    =>  'El código del departamneto es requerido'
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * 
     */
    public function sheets(): array
    {
        return [
            0 => $this
        ];
    }
}
