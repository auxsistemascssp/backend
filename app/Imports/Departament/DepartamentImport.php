<?php

namespace App\Imports\Departament;

use App\Models\Departament;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DepartamentImport implements ToModel, WithValidation, WithStartRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Departament([
            'name'      =>  $row[0],
        ]);
    }

    /**
     * 
     */
    public function rules(): array
    {
        return [
            '0' =>  'required|unique:departaments,name',
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '0.required'    =>  'El nombres e requerido',
            '0.unique'      =>  'El nombre ya esta en uso'
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
