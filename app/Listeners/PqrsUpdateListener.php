<?php

namespace App\Listeners;

use App\Events\PqrsUpdateEvent;
use App\Mail\pqrs\PqrsUpdateMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class PqrsUpdateListener implements ShouldQueue
{

    

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PqrsUpdateEvent $event)
    {
        Mail::to($event->pqrs->email)->send(new PqrsUpdateMail($event->pqrs));
    }
}
