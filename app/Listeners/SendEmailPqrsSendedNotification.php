<?php

namespace App\Listeners;

use App\Events\PqrsSended;
use App\Mail\PqrsSendedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailPqrsSendedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PqrsSended  $event
     * @return void
     */
    public function handle(PqrsSended $event)
    {   
        Mail::to($event->pqrs->email)->send(new PqrsSendedMail($event->pqrs));
    }
}
