<?php

namespace App\Listeners;

use App\Events\EmployeeUpdated;
use App\Mail\CondolenceEmployeeMail;
use App\Models\Employee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class EmployeeUpdatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\EmployeeUpdated  $event
     * @return void
     */
    public function handle(EmployeeUpdated $event)
    {
        // $emails = Employee::with('user')->get()->pluck('user.email');

        // if($event->employee->employee_state_id == 3){
        //     Mail::to(['stiwart94@gmail.com', 'jstwartvalencia@unipacifico.edu.co', 'jsvalencia55@misena.edu.co', 'joe.valencia@correounivalle.edu.co'])
            // Mail::to($emails)
            // ->send(new CondolenceEmployeeMail($event->employee));
        // }
    }
}
