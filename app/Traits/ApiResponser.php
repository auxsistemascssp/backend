<?php

namespace App\Traits;

trait ApiResponser {

    public function successResponse($data = [], $code = 200)
    {
        return response()->json([
            'data'  =>  $data,
        ], $code);
    }

    public function errorResponse($data = [], $message, $code = 400)
    {
        return response()->json([
            'data'      =>  $data,
            'message'   =>  $message
        ], $code);
    }
}