<?php 

namespace App\Traits;

use App\Exceptions\ThereIsNotInternetException;
use Illuminate\Support\Facades\Storage;
use Intervention\Image;

use Image as ImageFacade;

trait HasCover
{
    public function loadCover($file, $path, $field, $name = null)
    {
        $newName = is_null($name) ? time() : $name;

        $filename = $newName.'.'.$file->getClientOriginalExtension();

        $readPath = $path;
        $savePath = config('filesystems.disks.public')['root']."/public/".$readPath.'/'.$filename;

        //
        $this->{$field} = $readPath.'/'.$filename;

        return Storage::disk('public')->put(
            $readPath.'/'.$filename,
            file_get_contents($file),
            'public'
        );
    }

    /**
     * 
     */
    public function loadCoverDefault($path, $name, $text = null)
    {
        $filename = $name.'.jpg';
        $textImage = is_null($text) ? $name : $text; 
        $readPath = $path;
        
        $savePath = config('filesystems.disks.public')['root'].'/'.$readPath.'/'.$filename;
        $this->cover = $readPath.'/'.$filename;
        
        if(!Storage::disk('public')->exists('users/'.$filename)){
            $image = $this->getTextImage($textImage);
            $image->save($savePath, 72);
        }
        
        
        return true;

    }

    private function getTextImage($text)
    {
        $img = ImageFacade::canvas(250, 250)
            ->circle(245, 125, 125, function($draw){
                $draw->border(1, '#003A5E');
                $draw->background("#0071B7");
            })
            ->text($text, 120, 155, function($font){
            $font->file(storage_path('fonts/roboto_bold.ttf'));
            $font->size(90);
            $font->color('#cce3f1');
            $font->align('center');
        });

        return $img;
    }
    
    public function deleteCover($field)
    {
        $pathFile = str_replace(env('APP_URL').'/', "", $this->{$field});
        
        if(Storage::disk('public')->delete($pathFile)){
            $this->{$field}= "";
            return true;
        }

        return false;
    }
}