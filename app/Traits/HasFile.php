<?php

namespace App\Traits;

use Exception;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasFile
{
    public function loadFile(UploadedFile $file, $folder, $name = null)
    {
        $filename = $name.time().rand(0, 6).'.'.$file->getClientOriginalExtension();

        $readPath = $folder;
        $savePath = 'public/'.$readPath.'/'.$filename;
        
        if(Storage::put($savePath, file_get_contents($file), 'public')){
            $finalUrl = env('APP_URL').'/'.$readPath.'/'.$filename;
            return $finalUrl;
        }

        throw new Exception("No se pudo cargar el video");
    }

    public function removeFile($fileUrl)
    {
        $pathFile = str_replace(env('APP_URL').'/', "", $fileUrl);
        
        return Storage::delete('public/'.$pathFile);
    }
}