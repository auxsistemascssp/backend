<!DOCTYPE html>

<html>
<head>
    <title>CCSP</title>
</head>
<body>
    <p>Hola <b>{{ $pqrs->user_name }}</b></p>
    <p>
        Hemos recibido tu solicitud de tipo <b>{{ $pqrs->type->name }}</b> para el area de <b>{{ $pqrs->departament->name }}</b>, actualmente el estado de tu solicitud esta <b>{{ $pqrs->state->name }}</b>, conforme se avance su solicitud se le informará mediante este canal de comunicación, támbien puedes consultar el estado de tu solicitud con el siguiente codigo.
    </p>
    <p>
        Codigo de radicado: <b>{{$pqrs->id}}</b>
    </p>
</html>