<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
   
    {{-- CSS --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">

    {{-- JS --}}
    <script src="https://kit.fontawesome.com/14524cb6ab.js" crossorigin="anonymous"></script>
</head>
<style>
    *{
        padding: 0;
        margin: 0;
    }

    body {
        font-family: 'Roboto', sans-serif;
    }

    .container{
        padding: 0.5rem;
        background-color: #e4f7ff;
        position: relative;
        overflow: hidden;
        display: flex;
        align-items: center;
    }

    .content_message {
        border: 6px solid #cccccc;
        padding: 1rem;
        flex-grow: 1;
        z-index: 0;
    }

        .d-flex{
            display: flex;
        }

        .align-items-center{
            align-items: center;
        }

    .employee_name {
        font-family: 'Great Vibes', cursive;
        font-size: 4rem;
    }

    .employee_content{
        align-items: center;
        color: white;
        display: flex;
        justify-content: space-between;
        margin-top: -2rem;
    }

    .bg_employee_text{
        height: 10rem;
        background-color: #0071B7;
        width: 100%;
        position: absolute;
        right: 0;
        left: 0;
    }

    .img_fluid{
        width: 100%;
    }

    .ips_title{
        margin-top: 2rem;
        font-size: 1.2rem;
        font-family: 'Roboto', sans-serif;
    }

    .text-center {
        text-align: center;
    }

    .employee_dates {
        display: flex;
        justify-content: center;
    }

    .employee_dates i {
        font-size: 0.6rem;
        margin-left: 0.4rem;
        margin-right: 0.4rem;
    }

    .employee_legend {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }

    .employee_legend {
        width: 50%;
        margin-top: -2rem;
    }
</style>
<body>
    <div class="container">
        <div class="bg_employee_text"></div>
        <div class="content_message">
            <div class="ips_title text-center">
                La clinica santa sofia del pacifico, lamenta el fallecimiento de
            </div>
            <div class="employee_content">
                <div class="">
                    <span class="employee_name">        
                        Pepito Perez Supongamosa
                    </span>
                    <div class="employee_dates d-flex text-center">
                        <span class="d-flex align-items-center">
                            <i class="fa-regular fa-calendar"></i>
                            Enero 01, 2001
                        </span>
                        <span class="divider_horizontal"></span>
                        <span class="d-flex align-items-center">
                            <i class="fa-solid fa-cross"></i>
                            Enero 01, 2011
                        </span>
                    </div>
                </div>
                <div class="employee_avatar">
                    <img  src="{{asset('img/user_avatar.png')}}" class="img_fluid" />
                </div>
            </div>
            <div class="employee_legend text-center">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, repellat quo quod est consequuntur optio, molestiae exercitationem qui, consectetur alias quasi porro.</p>
            </div>
        </div>
    </div>
</body>
</html>