<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="x-apple-disable-message-reformatting">
        
        {{-- CSS --}}
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">

        {{-- JS --}}
        <script src="https://kit.fontawesome.com/14524cb6ab.js" crossorigin="anonymous"></script>

        <!--[if mso]>
        <style>
            table {border-collapse:collapse;border-spacing:0;border:none;margin:0;}
            div, td {padding:0;}
            div {margin:0 !important;}
        </style>
        <noscript>
            <xml>
                <o:OfficeDocumentSettings>
                <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
        </noscript>
        <![endif]-->
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            /* body {
                background-color: #e4f7ff;
            } */

            .align-items-center{
                align-items: center;
            }

            .d-flex{
                display: flex;
            }

            .employee_name {
                font-family: 'Great Vibes', cursive;
                font-size: 4rem;
            }

            .employee_dates {
                justify-content: center;
            }

            .employee_dates span {
                margin: 0 0.4rem;
            }

            .employee_dates i {
                font-size: 0.6rem;
                margin: 0 0.3rem;
            }

            .ips_title{
                font-size: 1.5rem;
                font-family: 'Roboto', sans-serif;
                text-align: center;
            }

            .message{
                width: 50%;
                margin: 1rem auto;
            }

            .text-primary {
                color: #0071B7;
            }

            .text-bold{
                font-weight: 700;
            }

            span, h1, h2, h3, h4, h5, h6{
                margin: 1rem 0;
            }

            @media screen and (max-width: 799px) {
                
                .employee_name{
                    font-size: 2.5rem;
                }

                .ips_title{
                    font-size: 1.2rem;
                }

                .message{
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
        <table bgcolor="#e4f7ff" class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="padding: 1rem;">
            <tbody>
                <tr>
                    <td>
                        <table celpadding="0" cellspacing="0" align="center" width="100%">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <span class="ips_title text-primary">La <b>clínica santasofia</b> lamenta el fallecimiento de</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" align="center">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <div style="position: relative;">
                                            <img 
                                                src="https://cdn3.iconfinder.com/data/icons/vector-icons-6/96/256-512.png"
                                                style="border-radius: 50%"
                                                width="150"
                                            />
                                            {{-- <img 
                                                src="https://clinicasantasofia.com/ribbon.svg"
                                                width="20"
                                                style="position: absolute;transform: translate(-193%, -50%); bottom: 0;"
                                            /> --}}
                                        </div>
                                    </td> 
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <span class="employee_name text-primary">        
                                            {{$employee->user->name}} {{ $employee->user->last_name}}
                                        </span>
                                        <h3 style="margin: 0">
                                            (<b>{{$employee->charge->name}}</b>)
                                        </h3>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" align="center">
                            <tbody>
                                <tr >
                                    <td style="text-align: center">
                                        {{-- <div class="employee_dates text-center"> --}}
                                            <span style="margin: 0">
                                                🗓
                                                {{ $employee->getFormatDate('birthday')}}
                                            </span>
                                            -
                                            <span class="">
                                                ✝
                                                {{ $employee->getFormatDate('dead_day') }}
                                            </span>
                                        {{-- </div> --}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="message" align="center">
                            <tbody>
                                <tr>
                                    <td style="text-align: center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, repellat quo quod est consequuntur optio, molestiae exercitationem qui, consectetur alias quasi porro.</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td style="text-align: center">
                                        <img src="https://prueba.clinicasantasofia.com/img/logo.png" width="150"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>