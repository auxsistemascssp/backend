<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="x-apple-disable-message-reformatting">
        <link rel="stylesheet" href="{{asset('css/app.css')}}"/>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <!--[if mso]>
        <style>
            table {border-collapse:collapse;border-spacing:0;border:none;margin:0;}
            div, td {padding:0;}
            div {margin:0 !important;}
        </style>
        <noscript>
            <xml>
                <o:OfficeDocumentSettings>
                <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
        </noscript>
        <![endif]-->
        <style>

            .bg-primary{
                background-color: #0071B7 !important;
            }

            .text-white{
                color: white;
            }

            .birthday-title{
                font-size: 4rem;
                color: #00446e;
            }

            .birthday-user{
                font-weight: 700;
                font-size: 2.5rem;
            }
        </style>
    </head>
    <body>
        <table cellspacing="0" cellpadding="0" style="width: 100%">
            <thead style="background-color: #e9edf8;">
                <tr>
                    <th style="text-align: center;font-size: 1.2rem;height: 50px;">
                    <span class="birthday-title">
                        <span style="font-weight: 400;">
                            Feliz Cumpleaños
                        </span>
                        <span
                        >
                          {{$employee->user->name}}
                        </span>
                    </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color: #cacde2;">
                    <td style="text-align:center;">
                        <img 
                            src="https://blog.bonus.ly/hubfs/celebrating-employee-birthday.png"
                            style="width: 100%;height: auto;"
                        >
                    </td>
                </tr>
                <tr style="background-color: #cacde2;">
                    <td style="padding: 0 3rem;">
                        <p style="font-size: 1.5rem;color:#363636">
                        La clinica santasofia del pacifico te desea que tengas un grandioso día lleno de incontables éxitos y abundante salud, donde tus razones para sonreír se multipliquen y la felicidad rebose para que puedas compartir en compañía de tus seres queridos.
                        </p>
                    </td>
                </tr>
                <tr></tr>
            </tbody>
        </table>
    </body>
</html>