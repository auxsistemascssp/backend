<!DOCTYPE html>

<html>
<head>
    <title>CCSP</title>
</head>
<body>
    <p>Hola <b>{{ $pqrs->user_name }}</b></p>
    <p>
        Tu solicitud con codigo de radicado <b>{{ $pqrs->id}}</b> de tipo <b>{{ $pqrs->type->name }}</b> ahora tiene el estado <b>{{ $pqrs->state->name }}</b>, conforme se avance su solicitud se le informará mediante este canal de comunicación.
    </p>

    @if(strlen($pqrs->response) > 5)
        <p>Respuesta: </p>
        {!! $pqrs->response !!}
    @endif
</html>