<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('identification_number')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->unsignedInteger('gender_id')->nullable();
            $table->string('username');
            $table->string('password');
            $table->string('cover')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('gender_id')->references('id')
            ->on('genders')->onDelete('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
