<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('charge_id');
            $table->unsignedInteger('employee_state_id')
            ->nullable()
            ->default(true);

            $table->string('phone', 20)->nullable();
            $table->date('birthday');
            $table->date('dead_day')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
            ->on('users')->onDelete('cascade');

            $table->foreign('charge_id')->references('id')
            ->on('charges')->onDelete('cascade');

            $table->foreign('employee_state_id')->references('id')
            ->on('employee_states')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
