<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('to');
            $table->string('target')->default('_self');
            $table->boolean('is_linked_home')->default(false);
            $table->string('cover')->nullable();
            $table->unsignedInteger('link_category_id');
            $table->timestamps();

            $table->foreign('link_category_id')->references('id')
            ->on('link_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
