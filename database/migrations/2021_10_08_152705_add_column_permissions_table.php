<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->after('guard_name', function($table){
                $table->string('description')->nullable();
                $table->unsignedInteger('category_permission_id');
                $table->unsignedInteger('crud_action_id');

                $table->foreign('category_permission_id')->references('id')
                ->on('category_permissions')->onDelete('cascade');
                
                $table->foreign('crud_action_id')->references('id')
                ->on('crud_actions')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            // $table->dropColumn('description');
            // $table->dropColumn('category_permission_id');
            // $table->dropColumn('crud_action_id');
        });
    }
}
