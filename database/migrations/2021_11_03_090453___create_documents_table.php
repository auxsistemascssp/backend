<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('src');
            $table->string('code', 20)->nullable();
            $table->unsignedInteger('document_type_id')->nullable();
            $table->unsignedInteger('departament_id')->nullable();
            $table->unsignedInteger('process_id')->nullable();
            $table->unsignedInteger('version')->default(1);
            $table->boolean('state')->default(true);
            $table->enum('origin', ['externo', 'interno'])->nullable();
            $table->timestamps();

            $table->foreign('document_type_id')->references('id')
            ->on('document_types')->onDlete('cascade');

            $table->foreign('departament_id')->references('id')
            ->on('departaments')->onDlete('cascade');

            $table->foreign('process_id')->references('id')
            ->on('processes')->onDlete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
