<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePqrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pqrs', function (Blueprint $table) {
            $table->id();
            $table->string('user_name')->nullable();
            $table->unsignedInteger('pqrs_type_id');
            $table->unsignedInteger('departament_id');
            $table->unsignedInteger('pqrs_state_id')->default(1);
            $table->string('email', 50)->nullable();
            $table->text('description');
            $table->text('response')->nullable();
            $table->timestamps();

            $table->foreign('pqrs_type_id')->references('id')
            ->on('pqrs_types')->onDelete('cascade');

            $table->foreign('departament_id')->references('id')
            ->on('departaments')->onDelete('cascade');

            $table->foreign('pqrs_state_id')->references('id')
            ->on('pqrs_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pqrs');
    }
}
