<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        User::factory()->create([
            'name'       =>  'Auxiliar',    
            'last_name'  =>  'Sistemas',    
            'email'      =>  'auxsistemas@clinicasantasofia.com',
            'password'   =>   bcrypt('Sistemas123*')  
        ])->each(function(User $user){
            $user->assignRole(1);
        });
    }
}
