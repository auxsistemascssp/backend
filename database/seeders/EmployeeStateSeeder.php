<?php

namespace Database\Seeders;

use App\Models\EmployeeState;
use Illuminate\Database\Seeder;

class EmployeeStateSeeder extends Seeder
{
    /**
     * 
     */
    private $states = [
        'inactivo', 'activo', 'fallecido'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->states as $key => $state)
        {
            EmployeeState::create([
                'name'  =>  $state
            ]);
        }
    }
}
