<?php

namespace Database\Seeders;

use App\Models\PqrsType;
use Illuminate\Database\Seeder;

class PqrsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PqrsType::create(['name' => 'petición']);
        PqrsType::create(['name' => 'queja']);
        PqrsType::create(['name' => 'reclamo']);
        PqrsType::create(['name' => 'denuncias']);
        PqrsType::create(['name' => 'sugerencia']);
        PqrsType::create(['name' => 'felicitación']);
    }
}
