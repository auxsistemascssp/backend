<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * 
     */
    private $seeders = [
        'migration',
        'crudActions',
        'categoryPermission',
        'roles',
        'genders',
        'users',
        'postTypes',
        'postStates',
        'posts',
        'pqrsTypes',
        'resourceTypes',
        'pqrsStates',
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->seeders as $seed) {
            $this->command->line("Processing {$seed}");
            call_user_func([$this, $seed]);
        }
    }

    /**
     * 
     */
    private function migration()
    {
        //$this->command->call('key:generate');
        $this->command->call('migrate:refresh');
        $this->command->line('Migrated tables.');
    }

    /**
     * 
     */
    private function crudActions()
    {
        $this->call(CrudActionSeeder::class);
    }
    
    /**
     * 
     */
    private function categoryPermission()
    {
        $this->call(CategoryPermissionSeeder::class);
    }

    /**
     * 
     */
    private function roles()
    {
        $this->call(RolAndPermissionSeeder::class);
    }

    /**
     * 
     */
    private function users()
    {
        $this->call(UserSeeder::class);
    }

    /**
     * 
     */
    private function postTypes()
    {
        $this->call(PostTypeSeeder::class);
    }

    /**
     * 
     */
    public function postStates()
    {
        return $this->call(PostStateSeeder::class);
    }

    /**
     * 
     */
    public function posts()
    {
        return $this->call(PostSeeder::class);
    }

    /**
     * 
     */
    public function pqrsTypes()
    {
        return $this->call(PqrsTypeSeeder::class);
    }

    /**
     * 
     */
    public function pqrsStates()
    {
        return $this->call(PqrsStateSeeder::class);
    }
    
    /**
     * 
     */
    public function genders()
    {
        return $this->call(GenderSeder::class);
    }
    
    /**
     * 
     */
    public function resourceTypes()
    {
        return $this->call(ResourceTypeSeeder::class);
    }
}
