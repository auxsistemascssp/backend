<?php

namespace Database\Seeders;

use App\Models\CrudAction;
use Illuminate\Database\Seeder;

class CrudActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            [ 
                'name'      =>  'todo',
                'action'    =>  '*',
            ],
            [ 
                'name'      =>  'buscar',
                'action'    =>  'search',
            ],
            [ 
                'name'      =>  'crear',
                'action'    =>  'create',
            ],
            [ 
                'name'      =>  'editar',
                'action'    =>  'edit',
            ],
            [ 
                'name'      =>  'eliminar',
                'action'    =>  'delete',
            ]
        ];

        foreach($actions as $action){
            CrudAction::create($action);
        }
    }
}
