<?php

namespace Database\Seeders;

use App\Models\CategoryPermission;
use App\Models\CrudAction;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class RolAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create([
            'name'          =>  'admin',
        ]);


        $permissionCategories = CategoryPermission::where('id', '>', 1)->get();
        $crudActions = CrudAction::where('id', '>', 1)->get();

        $allPermissions = Permission::create([
            'name'                   => '*',
            'description'            =>  "",
            'category_permission_id' =>  1,
            'crud_action_id'         =>  1,
        ]);

        foreach($permissionCategories as $category){
            foreach($crudActions as $action){
                Permission::create([
                    'name'                   =>  "{$action->name} {$category->name}",
                    'guard_name'             =>  'sanctum',
                    'description'            =>  "",
                    'category_permission_id' =>  $category->id,
                    'crud_action_id'         =>  $action->id,
                ]);
            }
        }

        $adminRole->givePermissionTo($allPermissions);
    }
}
