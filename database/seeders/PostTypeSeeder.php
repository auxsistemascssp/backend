<?php

namespace Database\Seeders;

use App\Models\PostType;
use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'code'  =>  'blog',
                'name'  =>  'blog',
            ],
            [
                'code'  =>  'ad',
                'name'  =>  'anuncio',
            ],
            [
                'code'  =>  'event',
                'name'  =>  'evento',
            ],
            [
                'code'  =>  'ad_popup',
                'name'  =>  'anuncio emergente',
            ]
        ];

        foreach($types as $type) {
            PostType::create($type);
        }
    }
}
