<?php

namespace Database\Seeders;

use App\Models\CategoryPermission;
use Illuminate\Database\Seeder;

class CategoryPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            [
                'name'      =>  'todo',
                'subject'   =>  '*'
            ],
            [
                'name'      =>  'usuario',
                'subject'   =>  'user'
            ],
            [
                'name'      =>  'departamentos',
                'subject'   =>  'departament'
            ],
            [
                'name'      =>  'cargo',
                'subject'   =>  'charge'
            ],
            [
                'name'      =>  'extensión',
                'subject'   =>  'phone'
            ],
            [
                'name'      =>  'empleado',
                'subject'   =>  'employee'
            ],
            [
                'name'      =>  'noticia',
                'subject'   =>  'post'
            ],
            [
                'name'      =>  'evento',
                'subject'   =>  'event'
            ],
            [
                'name'      =>  'anuncio',
                'subject'   =>  'ad'
            ],
            [
                'name'      =>  'categoria vinculo',
                'subject'   =>  'link category'
            ],
            [
                'name'      =>  'vinculo',
                'subject'   =>  'link'
            ],
            [
                'name'      =>  'banner',
                'subject'   =>  'carousel'
            ],
            [
                'name'      =>  'video',
                'subject'   =>  'video'
            ],
            [
                'name'      =>  'foto',
                'subject'   =>  'photo'
            ],
            [
                'name'      =>  'pqrs',
                'subject'   =>  'pqrs'
            ],
            [
                'name'      =>  'documentos',
                'subject'   =>  'document'
            ],
        ];

        foreach($subjects as $subject){
            CategoryPermission::create($subject);
        }
    }
}
