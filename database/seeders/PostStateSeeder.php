<?php

namespace Database\Seeders;

use App\Models\PostState;
use Illuminate\Database\Seeder;

class PostStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'code'  =>  'publish',
                'name'  =>  'publico',
                'bg_color'      =>  '#0087DB',
                'text_color'    =>  '#fff',
            ],
            [
                'code'  =>  'private',
                'name'  =>  'privado',
                'bg_color'      =>  '#198754',
                'text_color'    =>  '#fff',
            ],
            // [
            //     'code'  =>  'draft',
            //     'name'  =>  'borrador',
            //     'bg_color'      =>  '#6f42c1',
            //     'text_color'    =>  '#fff',
            // ],
            // [
            //     'code'  =>  'pending',
            //     'name'  =>  'pendiente',
            //     'bg_color'      =>  '#fd7e14',
            //     'text_color'    =>  '#000',
            // ],
            // [
            //     'code'  =>  'trash',
            //     'name'  =>  'eliminado',
            //     'bg_color'      =>  '#dc3545',
            //     'text_color'    =>  '#fff',
            // ],
            // [
            //     'code'  =>  'auto_save',
            //     'name'  =>  'auto guardado',
            //     'bg_color'      =>  '#198754',
            //     'text_color'    =>  '#fff',
            // ]
        ];

        foreach($types as $type) {
            PostState::create($type);
        }
    }
}
