<?php

namespace Database\Seeders;

use App\Models\PqrsState;
use Illuminate\Database\Seeder;

class PqrsStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PqrsState::create([
            'name'          => 'pendiente',
            'bg_color'      => '#fd7e14', 
            'text_color'    => '#000'
        ]);
        
        PqrsState::create([
            'name'          => 'en proceso',
            'bg_color'      => '#0d6efd', 
            'text_color'    => '#fff'
        ]);
        
        PqrsState::create([
            'name'          => 'terminada',
            'bg_color'      => '#198754', 
            'text_color'    => '#fff'
        ]);
        
        PqrsState::create([
            'name'          => 'cancelada',
            'bg_color'      => '#dc3545', 
            'text_color'    => '#fff'
        ]);
    }
}
