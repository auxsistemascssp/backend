<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(5);
        $slug = str_replace(" ", "-", $name);
        $users = User::whereHas('roles')->get()->pluck('id');
        $body = $this->faker->paragraph();

        return [
            'title'                 =>  $name,
            'slug'                  =>  $slug,
            'body'                  =>  $body,
            'short_description'     =>  substr($body, 0, 40),
            'cover'                 =>  $this->faker->imageUrl(),
            'user_id'               =>  $users->random(),
            'post_type_id'          =>  1,
            'post_state_id'         =>  1
        ];
    }
}
